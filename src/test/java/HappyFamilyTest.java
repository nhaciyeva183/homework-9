import controller.FamilyController;
import dao.Family;
import modules.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

class HappyFamilyTest {

    private final static FamilyController familyController = new FamilyController();

    @Test
    void displayAllFamilies() {
        Human mother = new Human("Anna", "Bell", 35);
        Human father = new Human("George", "Bell", 40);


        Set<String> habits = new HashSet<>();
        habits.add("eat");
        habits.add("sleep");
        habits.add("drink");

        Pet cat = new DomesticCat("Kiwi", 2, 60, habits);
        Pet dog = new Dog("Coco", 3, 30, habits);

        Set<Pet> pets = new HashSet<>();
        pets.add(cat);
        pets.add(dog);

        Human child = new Human("Mary", "Bell", 15);
        Human child2 = new Human("Maryam", "Bell", 12);
        Human child3 = new Human("John", "Bell", 7);


        List<Human> children = new ArrayList<>();
        children.add(child);
        children.add(child2);
        children.add(child3);
        Family family = new Family(mother, father, children, pets);


        Human mother2 = new Human("Catherine", "Johnson", 23);
        Human father2 = new Human("Neil", "Johnson", 28);


        Set<String> habits2 = new HashSet<>();
        habits2.add("play");
        habits2.add("speak");
        habits2.add("walk");

        Pet robocat = new RoboCat("Robox", 1, 100, habits);
        Set<Pet> pets2 = new HashSet<>();
        pets2.add(robocat);

        Human child_family2 = new Human("Amelia", "Johnson", 5);

        List<Human> children2 = new ArrayList<>();
        children2.add(child_family2);


        Family family2 = new Family(mother2, father2, children2, pets2);


        List<Family> families = familyController.getAllFamilies();
        families.add(family);
        families.add(family2);
        Assertions.assertTrue(families.contains(family) && families.contains(family2));
    }


    @Test
    void count() {
        Human mother = new Human("Anna", "Bell", 35);
        Human father = new Human("George", "Bell", 40);


        Set<String> habits = new HashSet<>();
        habits.add("eat");
        habits.add("sleep");
        habits.add("drink");

        Pet cat = new DomesticCat("Kiwi", 2, 60, habits);
        Pet dog = new Dog("Coco", 3, 30, habits);

        Set<Pet> pets = new HashSet<>();
        pets.add(cat);
        pets.add(dog);

        Human child = new Human("Mary", "Bell", 15);
        Human child2 = new Human("Maryam", "Bell", 12);
        Human child3 = new Human("John", "Bell", 7);


        List<Human> children = new ArrayList<>();
        children.add(child);
        children.add(child2);
        children.add(child3);
        Family family = new Family(mother, father, children, pets);


        Human mother2 = new Human("Catherine", "Johnson", 23);
        Human father2 = new Human("Neil", "Johnson", 28);


        Set<String> habits2 = new HashSet<>();
        habits2.add("play");
        habits2.add("speak");
        habits2.add("walk");

        Pet robocat = new RoboCat("Robox", 1, 100, habits);
        Set<Pet> pets2 = new HashSet<>();
        pets2.add(robocat);

        Human child_family2 = new Human("Amelia", "Johnson", 5);

        List<Human> children2 = new ArrayList<>();
        children2.add(child_family2);


        Family family2 = new Family(mother2, father2, children2, pets2);


        List<Family> families = familyController.getAllFamilies();
        families.add(family);
        families.add(family2);

        Assertions.assertEquals(2, families.size());
    }

    @Test
    void getFamilyByIndex() {
        Human mother = new Human("Anna", "Bell", 35);
        Human father = new Human("George", "Bell", 40);


        Set<String> habits = new HashSet<>();
        habits.add("eat");
        habits.add("sleep");
        habits.add("drink");

        Pet cat = new DomesticCat("Kiwi", 2, 60, habits);
        Pet dog = new Dog("Coco", 3, 30, habits);

        Set<Pet> pets = new HashSet<>();
        pets.add(cat);
        pets.add(dog);

        Human child = new Human("Mary", "Bell", 15);
        Human child2 = new Human("Maryam", "Bell", 12);
        Human child3 = new Human("John", "Bell", 7);


        List<Human> children = new ArrayList<>();
        children.add(child);
        children.add(child2);
        children.add(child3);
        Family family = new Family(mother, father, children, pets);


        Human mother2 = new Human("Catherine", "Johnson", 23);
        Human father2 = new Human("Neil", "Johnson", 28);


        Set<String> habits2 = new HashSet<>();
        habits2.add("play");
        habits2.add("speak");
        habits2.add("walk");

        Pet robocat = new RoboCat("Robox", 1, 100, habits);
        Set<Pet> pets2 = new HashSet<>();
        pets2.add(robocat);

        Human child_family2 = new Human("Amelia", "Johnson", 5);

        List<Human> children2 = new ArrayList<>();
        children2.add(child_family2);


        Family family2 = new Family(mother2, father2, children2, pets2);


        List<Family> families = familyController.getAllFamilies();
        families.add(family);
        families.add(family2);

        Family family0Index = familyController.getFamilyByIndex(0);
        Assertions.assertEquals(0, families.indexOf(family0Index));
    }

    @Test
    void deleteFamilyByIndex() {
        Human mother = new Human("Anna", "Bell", 35);
        Human father = new Human("George", "Bell", 40);


        Set<String> habits = new HashSet<>();
        habits.add("eat");
        habits.add("sleep");
        habits.add("drink");

        Pet cat = new DomesticCat("Kiwi", 2, 60, habits);
        Pet dog = new Dog("Coco", 3, 30, habits);

        Set<Pet> pets = new HashSet<>();
        pets.add(cat);
        pets.add(dog);

        Human child = new Human("Mary", "Bell", 15);
        Human child2 = new Human("Maryam", "Bell", 12);
        Human child3 = new Human("John", "Bell", 7);


        List<Human> children = new ArrayList<>();
        children.add(child);
        children.add(child2);
        children.add(child3);
        Family family = new Family(mother, father, children, pets);


        Human mother2 = new Human("Catherine", "Johnson", 23);
        Human father2 = new Human("Neil", "Johnson", 28);


        Set<String> habits2 = new HashSet<>();
        habits2.add("play");
        habits2.add("speak");
        habits2.add("walk");

        Pet robocat = new RoboCat("Robox", 1, 100, habits);
        Set<Pet> pets2 = new HashSet<>();
        pets2.add(robocat);

        Human child_family2 = new Human("Amelia", "Johnson", 5);

        List<Human> children2 = new ArrayList<>();
        children2.add(child_family2);


        Family family2 = new Family(mother2, father2, children2, pets2);


        List<Family> families = familyController.getAllFamilies();
        families.add(family);
        families.add(family2);


        Assertions.assertTrue(familyController.deleteFamilyByIndex(0));
    }

    @Test
    void deleteFamily() {
        Human mother = new Human("Anna", "Bell", 35);
        Human father = new Human("George", "Bell", 40);


        Set<String> habits = new HashSet<>();
        habits.add("eat");
        habits.add("sleep");
        habits.add("drink");

        Pet cat = new DomesticCat("Kiwi", 2, 60, habits);
        Pet dog = new Dog("Coco", 3, 30, habits);

        Set<Pet> pets = new HashSet<>();
        pets.add(cat);
        pets.add(dog);

        Human child = new Human("Mary", "Bell", 15);
        Human child2 = new Human("Maryam", "Bell", 12);
        Human child3 = new Human("John", "Bell", 7);


        List<Human> children = new ArrayList<>();
        children.add(child);
        children.add(child2);
        children.add(child3);
        Family family = new Family(mother, father, children, pets);


        Human mother2 = new Human("Catherine", "Johnson", 23);
        Human father2 = new Human("Neil", "Johnson", 28);


        Set<String> habits2 = new HashSet<>();
        habits2.add("play");
        habits2.add("speak");
        habits2.add("walk");

        Pet robocat = new RoboCat("Robox", 1, 100, habits);
        Set<Pet> pets2 = new HashSet<>();
        pets2.add(robocat);

        Human child_family2 = new Human("Amelia", "Johnson", 5);

        List<Human> children2 = new ArrayList<>();
        children2.add(child_family2);


        Family family2 = new Family(mother2, father2, children2, pets2);


        List<Family> families = familyController.getAllFamilies();
        families.add(family);
        families.add(family2);


        Assertions.assertTrue(familyController.deleteFamily(family2));
    }


    @Test
    void deleteAllChildrenOlderThan() {
        Human mother = new Human("Anna", "Bell", 35);
        Human father = new Human("George", "Bell", 40);


        Set<String> habits = new HashSet<>();
        habits.add("eat");
        habits.add("sleep");
        habits.add("drink");

        Pet cat = new DomesticCat("Kiwi", 2, 60, habits);
        Pet dog = new Dog("Coco", 3, 30, habits);

        Set<Pet> pets = new HashSet<>();
        pets.add(cat);
        pets.add(dog);

        Human child = new Human("Mary", "Bell", 15);
        Human child2 = new Human("Maryam", "Bell", 12);
        Human child3 = new Human("John", "Bell", 7);


        List<Human> children = new ArrayList<>();
        children.add(child);
        children.add(child2);
        children.add(child3);
        Family family = new Family(mother, father, children, pets);


        Human mother2 = new Human("Catherine", "Johnson", 24);
        Human father2 = new Human("Neil", "Johnson", 28);


        Set<String> habits2 = new HashSet<>();
        habits2.add("play");
        habits2.add("speak");
        habits2.add("walk");

        Pet robocat = new RoboCat("Robox", 1, 100, habits);
        Set<Pet> pets2 = new HashSet<>();
        pets2.add(robocat);

        Human child_family2 = new Human("Amelia", "Johnson", 16);

        List<Human> children2 = new ArrayList<>();
        children2.add(child_family2);


        Family family2 = new Family(mother2, father2, children2, pets2);


        List<Family> families = familyController.getAllFamilies();
        families.add(family);
        families.add(family2);

        familyController.deleteAllChildrenOlderThen(5);
        Assertions.assertTrue(family.getChildren().size() == 1 && family2.getChildren().size() == 0);
    }


}
