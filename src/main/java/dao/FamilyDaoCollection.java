package dao;

import modules.Human;
import modules.Pet;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class FamilyDaoCollection implements FamilyDao {
    private final List<Family> families = new ArrayList<>();


    @Override
    public List<Family> getAllFamilies() {
        return this.families;
    }

    @Override
    public Family getFamilyByIndex(int index) {
        return this.families.get(index);
//        if (index <= this.families.size()) {
//            return this.families.get(index);
//        } else return null;
    }

    @Override
    public Boolean deleteFamilyByIndex(int index) {
        if (index <= this.families.size()) {
            this.families.remove(index);
            return true;
        } else return false;
    }

    @Override
    public Boolean deleteFamily(Family family) {
        if (this.families.contains(family)) {
            return this.families.remove(family);
        } else return false;
    }

    @Override
    public void saveFamily(Family family) {
        if (this.families.contains(family)) {
            family = this.families.get(families.indexOf(family));
        } else this.families.add(this.families.size(), family);
    }

    @Override
    public void displayAllFamilies() {
        for (int i = 0; i < this.families.size(); i++) {
            System.out.println(i + " " + this.families.get(i));
        }
    }


    @Override
    public List<Family> getFamiliesBiggerThan(int number) {
        return this.families
                .stream()
                .filter(i -> i.countFamily() > number)
                .collect(Collectors.toList());
    }

    @Override
    public List<Family> getFamiliesLessThan(int number) {
        return this.families
                .stream()
                .filter(i -> i.countFamily() < number)
                .collect(Collectors.toList());
    }

    @Override
    public int countFamiliesWithMemberNumber(int number) {
        int count = 0;
        for (int i = 0; i < this.families.size(); i++) {
            if (this.families.get(i).countFamily() == i) {
                count++;
            }
        }
        return count;
    }

    @Override
    public Family createNewFamily(Human mother, Human father) {
        Family family = new Family();
        family.setMother(mother);
        family.setFather(father);
        saveFamily(family);
        return family;
    }


    @Override
    public void deleteAllChildrenOlderThen(int number) {
        for (int i = 0; i < this.families.size(); i++) {
            if (this.families.get(i).getChildren() != null) {
                for (int j = 0; j < families.get(i).getChildren().size(); j++) {

                    if ((2021 - this.families.get(i).getChildren().get(j).getYear()) > 15) {
                        this.families.get(i).getChildren().remove(j);
                    }
                }
            }
        }
    }

    @Override
    public int count() {
        return this.families.size();
    }

    @Override
    public Family getFamilyById(int index) {
        return this.families.get(index);
    }

    @Override
    public Set<Pet> getPets(int index) {

        return this.families.get(index).getPet();
    }

    @Override
    public void addPet(int index, Pet pet) {
        Set<Pet> pets = new HashSet<>();
        pets.add(pet);
        this.families.get(index).setPet(pets);
    }
}
