package dao;

import modules.Human;
import modules.Pet;

import java.util.List;
import java.util.Set;

public interface FamilyDao {
    List<Family> getAllFamilies();

    Family getFamilyByIndex(int index);

    Boolean deleteFamilyByIndex(int index);

    Boolean deleteFamily(Family family);

    void saveFamily(Family family);

    void displayAllFamilies();

    public List<Family> getFamiliesBiggerThan(int number);

    public List<Family> getFamiliesLessThan(int number);

    public int countFamiliesWithMemberNumber(int number);

    public Family createNewFamily(Human mother, Human father);

    public void deleteAllChildrenOlderThen(int number);

    public int count();

    public Family getFamilyById(int index);

    public Set<Pet> getPets(int index);

    public void addPet(int index, Pet pet);
}
