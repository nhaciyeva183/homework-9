package dao;


import modules.Human;
import modules.Pet;

import java.util.Arrays;
import java.util.List;
import java.util.Set;

public class Family {
    private Human mother;
    private Human father;
    private List<Human> children;
    private Set<Pet> pets;
    //private Human[] children;
    private Family family;

//    private Pet pet;


    public Family(Human mother, Human father) {
        this.mother = mother;
        this.father = father;
    }

    public Family(Human mother, Human father, List<Human> children) {
        this.mother = mother;
        this.father = father;
        this.children = children;
    }

    public Family(Human mother, Human father, List<Human> children, Set<Pet> pets) {
        this.mother = mother;
        this.father = father;
        this.children = children;
        this.pets = pets;
    }

    public Family() {
    }

    public Human getMother() {
        return mother;
    }

    public void setMother(Human mother) {

        this.mother = mother;
    }

    public Human getFather() {
        return father;
    }

    public void setFather(Human father) {
        this.father = father;
    }

    public List<Human> getChildren() {
        return children;
    }

    public void setChildren(List<Human> children) {
        this.children = children;
    }

    public Set<Pet> getPet() {
        return pets;
    }

    public void setPet(Set<Pet> pet) {
        this.pets = pet;
    }

    public boolean addChild(Family family, Human child) {
        family.getChildren().add(child);
        return true;
    }

    public int countFamily() {
        return 2 + (getChildren().size());
    }


    public boolean deleteChildByIndex(Family family, int index) {
        if (family.getChildren().get(index) != null) {
            family.getChildren().remove(index);
            return true;
        }
        return false;
    }

    public boolean deleteChild(Family family, Human child) {
        if (family.getChildren().contains(child)) {
            family.getChildren().remove(child);
            return true;
        }
        return false;
    }

    @Override
    public String toString() {
        return "Family{" + '\n' +
                "mother=" + mother + '\n' +
                ", father=" + father + '\n' +
                ", children=" + children + '\n' +
                ", pet=" + pets + '\n' +
                '}';
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);


//   public Human[] getChildren() {
//        return children;
//    }
//
//    public void setChildren(Human[] children) {
//        this.children = children;
//    }
//
//    public Pet getPet() {
//        return pet;
//    }
//
//    public void setPet(Pet pet) {
//        this.pet = pet;
//    }


//    public boolean addChild(Human child) {
//        Human[] children1 = new Human[children.length + 1];
//        for (int i = 0; i < children.length; i++) {
//            children1[i] = children[i];
//        }
//        children1[children.length] = child;
//        this.children = children1;
//
//        return true;
//    }
//

//
//    public int familyMembers() {
//        return 2 + (getChildren().length);
//    }


//    public boolean deleteChild(int index, Family family) {
//        if (children.length == 0 || index < 0 || index >= children.length) {
//            return true;
//        }
//        Human[] newChildren = new Human[children.length - 1];
//
//        for (int i = 0, k = 0; i < children.length; i++) {
//            if (i == index) continue;
//            newChildren[k++] = children[i];
//
//        }
//        this.children = newChildren;
//        return true;
//
//    }
//
//    public boolean deleteChild(Human human) {
//        Human[] newChildren = new Human[children.length - 1];
//        for (int i = 0, k = 0; i < children.length; i++) {
//            if (!children[i].equals(human)) continue;
//            newChildren[k++] = children[i];
//
//        }
//        this.children = newChildren;
//        return true;
//
//    }


    }

}
