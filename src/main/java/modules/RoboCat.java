package modules;

import java.util.Set;

public class RoboCat extends Pet {
    public RoboCat(String nickname, int age, int trickLevel, Set<String> habits) {
        super(nickname, age, trickLevel, habits);


    }

    @Override
    public void eat() {
        System.out.println("Robotic cat is eating");
    }

    @Override
    public void respond() {
        System.out.println("Robotic cat is designed to move and act like a real cat");
    }
}
