package modules;

public final class Man extends Human {
    public Man(String name, String surname, int year) {
        super(name, surname, year);
    }

    @Override
    public void greetPet(Pet pet) {
        super.greetPet(pet);
    }

    public void repairCar() {
        System.out.println("Today, i will repair my car");
    }
}
