package service;

import dao.Family;
import dao.FamilyDao;
import dao.FamilyDaoCollection;
import modules.Human;
import modules.Pet;


import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class FamilyService {
    private final FamilyDao familyDao = (FamilyDao) new FamilyDaoCollection();

    public List<Family> getAllFamilies() {
        return familyDao.getAllFamilies();
    }

    public Family getFamilyByIndex(int index) {
        if (index <= familyDao.getAllFamilies().size()) {
            return familyDao.getFamilyByIndex(index);
        }
        return null;
    }

    public Boolean deleteFamilyByIndex(int index) {
        if (index <= familyDao.getAllFamilies().size()) {
            familyDao.getAllFamilies().remove(index);
            return true;
        }
        return false;
    }

    public Boolean deleteFamily(Family family) {
        if (familyDao.getAllFamilies().contains(family)) {
            familyDao.getAllFamilies().remove(family);
            return true;
        }
        return false;
    }

    public void saveFamily(Family family) {
        familyDao.saveFamily(family);
    }

    public void displayAllFamilies() {
        familyDao.displayAllFamilies();
    }


    public List<Family> getFamiliesBiggerThan(int number) {
        return familyDao.getFamiliesBiggerThan(number);
    }

    public List<Family> getFamiliesLessThan(int number) {
        return familyDao.getFamiliesLessThan(number);
    }

    public int countFamiliesWithMemberNumber(int number) {
        return this.familyDao.countFamiliesWithMemberNumber(number);
    }

    public Family createNewFamily(Human mother, Human father) {
        return familyDao.createNewFamily(mother, father);
    }


    public void deleteAllChildrenOlderThen(int number) {
        familyDao.deleteAllChildrenOlderThen(number);
    }

    public int count() {
        return familyDao.count();
    }

    public Family getFamilyById(int index) {
        return familyDao.getFamilyById(index);
    }

    public Set<Pet> getPets(int index) {
        return familyDao.getPets(index);
    }

    public void addPet(int index, Pet pet) {
        familyDao.addPet(index, pet);
    }

}
