import controller.FamilyController;
import dao.Family;
import modules.*;

import java.util.*;

public class HappyFamily {
    private final static FamilyController familyController = new FamilyController();

    public static void main(String[] args) {
        /* ************************************************************************************************************************************************************************** */
        /*                                                                    FAMILY 1                                                                                       */
        Human mother = new Human("Anna", "Bell", 35);
        Human father = new Human("George", "Bell", 40);


        Set<String> habits = new HashSet<>();
        habits.add("eat");
        habits.add("sleep");
        habits.add("drink");

        Pet cat = new DomesticCat("Kiwi", 2, 60, habits);
        Pet dog = new Dog("Coco", 3, 30, habits);
        Set<Pet> pets = new HashSet<>();
        pets.add(cat);
        pets.add(dog);
        //System.out.println(cat);
        //System.out.println(pets);


        Map<String, String> schedule = new HashMap<>();
        schedule.put(DayOfWeek.SUNDAY.name(), "do home work");
        schedule.put(DayOfWeek.MONDAY.name(), " go to courses");
        schedule.put(DayOfWeek.TUESDAY.name(), "watch a film");
        schedule.put(DayOfWeek.WEDNESDAY.name(), "read a book");
        schedule.put(DayOfWeek.THURSDAY.name(), "go shopping");
        schedule.put(DayOfWeek.FRIDAY.name(), "complete course videos");
        schedule.put(DayOfWeek.SATURDAY.name(), "join online classes");

        Human child = new Human("Mary", "Bell", 15, 100, cat, schedule);
        Human child2 = new Human("Maryam", "Bell", 12);
        Human child3 = new Human("John", "Bell", 7);

        List<Human> children = new ArrayList<>();
        children.add(child);
        children.add(child2);
        children.add(child3);


        Family family = new Family(mother, father, children, pets);
        //System.out.println(family);

        /* ************************************************************************************************************************************************************************** */
        /*                                                                    FAMILY 2                                                                                */

        Human mother2 = new Human("Catherine", "Johnson", 23);
        Human father2 = new Human("Neil", "Johnson", 28);


        Set<String> habits2 = new HashSet<>();
        habits2.add("play");
        habits2.add("speak");
        habits2.add("walk");

        Pet robocat = new RoboCat("Robox", 1, 100, habits);
        Set<Pet> pets2 = new HashSet<>();
        pets2.add(robocat);

        Human child_family2 = new Human("Amelia", "Johnson", 5, 120, robocat, schedule);

        List<Human> children2 = new ArrayList<>();
        children2.add(child_family2);


        Family family2 = new Family(mother2, father2, children2, pets2);
        //System.out.println(family2);





        /*      ***********************************************************************************************************************************************************************
         ******************************************************************FAMILY CONTROLLER************************************************************************************
         ***********************************************************************************************************************************************************************
         */
        List<Family> families = familyController.getAllFamilies();
        families.add(family);
        families.add(family2);

        // Displays all Families' members;
        //familyController.displayAllFamilies();


        // Displays count of Families;
        //System.out.println(familyController.count());

        //Shows family whose index is 1;
        //System.out.println(familyController.getFamilyByIndex(1));


        //Delete  Family2;
//        familyController.deleteFamily(family2);    //delete using object
//        familyController.deleteFamilyByIndex(0);   //  delete using index
//        familyController.displayAllFamilies();


        //Delete older than 10 years children
//        familyController.deleteAllChildrenOlderThen(10);
//        familyController.displayAllFamilies();


        // Family members more than 3;
//        for (int i = 0; i < familyController.getFamiliesBiggerThan(3).size(); i++) {
//            System.out.println(familyController.getFamiliesBiggerThan(3).get(i));
//        }


        // Family members less tha 4;
//        for (int i = 0; i < familyController.getFamiliesLessThan(4).size(); i++) {
//            System.out.println(familyController.getFamiliesLessThan(4).get(i));
//        }


    }
}
